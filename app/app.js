
const React = require('react')
const ReactDOM = require('react-dom')
const antd = require('antd')

const MainLayout = require('./app/components/main_layout.js')

document.addEventListener("DOMContentLoaded", function(){
  elem = React.createElement(MainLayout, null, null);
  ReactDOM.render(elem, document.getElementById('root'));
});

