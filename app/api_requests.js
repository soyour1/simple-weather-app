
const https = require('https')

function getCurrentTimezoneOffsetInSeconds() {
  var now = new Date(Date.now());
  return - now.getTimezoneOffset() * 60;
}

function getPseudoTimezoneTimestamp(timestamp, timezone_offset) {
  return timestamp + timezone_offset - getCurrentTimezoneOffsetInSeconds();
}

function getPseudoTimezoneDate(timestamp, timezone_offset) {
  return new Date(
    getPseudoTimezoneTimestamp(timestamp, timezone_offset) * 1000
  );
}

function isSameDayOfMonth(timestamp1, timestamp2, timezone_offset) {
  var date1 = getPseudoTimezoneDate(timestamp1, timezone_offset);
  var date2 = getPseudoTimezoneDate(timestamp2, timezone_offset);
  return date1.getDate() === date2.getDate();
}

function processCurrentEntry(entry, timezone_offset) {
  return {
    timestamp: entry.dt,
    timezone_offset: timezone_offset,
    weather: {text: entry.weather[0].main, icon: entry.weather[0].icon},
    temp: entry.temp,
    pressure: entry.pressure,
    wind: {speed: entry.wind_speed, deg: entry.wind_deg},
  }
}

function processDailyEntry(entry, timezone_offset) {
  return {
    timestamp: entry.dt,
    timezone_offset: timezone_offset,
    weather: {text: entry.weather[0].main, icon: entry.weather[0].icon},
    temp: entry.temp,
    pressure: entry.pressure,
    wind: {speed: entry.wind_speed, deg: entry.wind_deg},
    hours: []
  };
}

function processHourlyEntry(entry, timezone_offset) {
  return {
    timestamp: entry.dt,
    timezone_offset: timezone_offset,
    weather: {text: entry.weather[0].main, icon: entry.weather[0].icon},
    temp: entry.temp,
    pressure: entry.pressure,
    wind: {speed: entry.wind_speed, deg: entry.wind_deg},
  };
}

function processWeatherData(inputData) {
  json = JSON.parse(inputData);
  
  if(!json.timezone_offset || !json.current || !json.daily || !json.hourly) {
    return undefined;
  }
  
  var current = processCurrentEntry(json.current, json.timezone_offset);

  var days = [];
  
  // process days
  days = json.daily.map(function(entry) {
    return processDailyEntry(entry, json.timezone_offset);
  });
  
  // process hours
  json.hourly.forEach(function(entry) {
    var hourly = processHourlyEntry(entry, json.timezone_offset);
    
    var day = days.find(function(day) {
      return isSameDayOfMonth(
        day.timestamp, hourly.timestamp, json.timezone_offset
      );
    });
    
    if(day) {
      day.hours.push(hourly);
    }
  });
  
  return {current: current, forecast: days};
}

function dailyForecastRequest(city, appid, callback) {
  
  var url_path = '/data/2.5/onecall' + 
    '?lat=' + city.coord.lat + 
    '&lon=' + city.coord.lon + 
    '&appid=' + appid;

  console.log(url_path);
  
  const request = https.request(
    {
      hostname: 'api.openweathermap.org',
      path: url_path,
      method: 'GET',
    },
    function (result) {
      var body = '';
    
      result.on('data', function (data) {
        body += data;
      });
      
      result.on('end', function() {
        callback(processWeatherData(body));
      });
    }
  );
  
  request.on('error', function(error) {
    console.error(error);
  });
  
  request.end();
}

module.exports = {
  dailyForecastRequest: dailyForecastRequest,
  getPseudoTimezoneDate: getPseudoTimezoneDate
};



