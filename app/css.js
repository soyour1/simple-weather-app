
cssId = 'antdcss';

function updateCSS(cssName) {
  // remove old CSS 
  var antdcss = document.getElementById(cssId);
  antdcss.remove(); 

  // Create new link Element 
  var link = document.createElement('link');  
  link.id = cssId;
  link.rel = 'stylesheet';  
  link.type = 'text/css'; 
  link.href = 'node_modules/antd/dist/' + cssName + '.css';  

  // Append link element to HTML head 
  var head = document.getElementsByTagName('HEAD')[0]; 
  head.appendChild(link);
}


module.exports = {
  updateCSS: updateCSS
}




