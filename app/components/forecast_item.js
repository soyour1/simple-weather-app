
const React = require('react')
const antd = require('antd')

const WeatherIconText = require('./weather_icon_text.js')
const TemperatureMinMax = require('./temperature_min_max.js')
const WindSpeed = require('./wind_speed.js')

const {getPseudoTimezoneDate} = require('../api_requests.js')

class ForecastItem extends React.Component {

  constructor(props) {
    super(props);
    
    this.weekday = new Array(7);
    this.weekday[0] = "Sun";
    this.weekday[1] = "Mon";
    this.weekday[2] = "Tue";
    this.weekday[3] = "Wed";
    this.weekday[4] = "Thu";
    this.weekday[5] = "Fri";
    this.weekday[6] = "Sat";
  }
  
  createDateElement(timestamp, timezone_offset, showDate, showHour) {
    var date = getPseudoTimezoneDate(timestamp, timezone_offset);
    var text = '';
    
    if(showDate) {
      text += this.weekday[date.getDay()] + ' ' + 
        date.getDate() + '/' + (date.getMonth()+1) + ' ';
    }
    if(showHour) {
      text += date.getHours() + 'h';
    }
    
    return React.createElement(
      'p',
      {style: {padding: '4px', margin: '0px', fontWeight: 'bold'}},
      text
    );
  }
  
  createWeatherElement(weather, size) {
    return React.createElement(
      WeatherIconText,
      {
        text: weather.text,
        icon: weather.icon,
        imgSize: size,
      },
      null
    );
  }
  
  createTemperatureElement(temp) {
    if(typeof temp === typeof {}) {
      return React.createElement(
        TemperatureMinMax,
        {
          min: temp.min,
          max: temp.max
        },
        null
      );
    }
    // else 
    return React.createElement(
      'span',
      {
        style: {
          fontSize: 'large',
          //fontWeight: 'bold'
        }
      },
      Math.round(temp - 273.15)
    );
  }
  
  createPressureElement(pressure) {
    return React.createElement(
      'span',
      {},
      pressure + ' hPa'
    );
  }
  
  createWindElement(speed, degrees) {
    return React.createElement(
      WindSpeed,
      {
        speed: speed,
        degrees: degrees,
      },
      null
    );
  }

  render() {
    var showDate = this.props.showDate || this.props.showHour;
    var children = [];
    
    if(showDate) {
      children.push(React.createElement(
        antd.Col,
        {key: 'date', span: 5},
        this.createDateElement(
          this.props.timestamp, 
          this.props.timezone_offset,
          this.props.showDate, 
          this.props.showHour)
      ));
    }
    
    children.push(React.createElement(
      antd.Col,
      {key: 'weather', span: showDate ? 6 : 8},
      this.createWeatherElement(this.props.weather, showDate ? 50 : 65)
    ));
    
    children.push(React.createElement(
      antd.Col,
      {key: 'temperature', span: showDate ? 4 : 5},
      this.createTemperatureElement(this.props.temp)
    ));
    
    children.push(React.createElement(
      antd.Col,
      {key: 'pressure', span: showDate ? 4 : 5},
      this.createPressureElement(this.props.pressure)
    ));
    
    children.push(React.createElement(
      antd.Col,
      {key: 'wind', span: showDate ? 5 : 6},
      this.createWindElement(this.props.wind.speed, this.props.wind.deg)
    ));
  
    return React.createElement(
      antd.Row,
      {
        align: 'middle',
        style: {width: '100%'}
      },
      children
    );
  }
  
}

module.exports = ForecastItem;



