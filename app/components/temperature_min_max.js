
const React = require('react')
const antd = require('antd')

var kelvinZero = 273.15;

class TemperatureMinMax extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {  
    var min = React.createElement(
      'div',
      {
        key: 'min',
        style: {
          textAlign: 'left',
          width: '40px',
          color: 'blue',
          fontSize: 'medium',
          //fontWeight: 'bold'
        }
      },
      Math.round(this.props.min - kelvinZero)
    );
    
    var max = React.createElement(
      'div',
      {
        key: 'max',
        style: {
          textAlign: 'left', 
          width: '40px',
          color: 'red',
          fontSize: 'large',
          //fontWeight: 'bold'
        }
      },
      Math.round(this.props.max - kelvinZero)
    );
  
    return React.createElement(
      antd.Space,
      {
        align: 'baseline'
      },
      [min, max]
    );
  }
  
}

module.exports = TemperatureMinMax;



