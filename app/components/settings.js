
const React = require('react')
const antd = require('antd')

const css = require('../css.js')

class Settings extends React.Component {

  constructor(props) {
    super(props);
    
    this.currentApiKey = this.props.initialApiKey;
  }
  
  onClose(validated) {
    // reset key if cancelled
    if(! validated) {
      this.currentApiKey = this.props.initialApiKey;
    }
    
    if(this.props.onClose) {
      this.props.onClose(this.currentApiKey);
    }
  }
  
  onThemeChange(checked) {
    if(checked) {
      css.updateCSS('antd.dark');
    } else {
      css.updateCSS('antd');
    }
  }
  
  onApiKeyChange(event) {
    var { value } = event.target;
    this.currentApiKey = value;
  }
  
  render() {
    var theme_switch = React.createElement(
      antd.Switch,
      {
        key: 'switch',
        onChange: this.onThemeChange.bind(this),
        style: {float: 'right'},
      },
      null
    );
    
    var theme_div = React.createElement(
      'div',
      {
        key: 'theme_div',
        style: {marginBottom: '16px'},
      },
      ['Dark theme', theme_switch]
    )
    
    var api_key_label = React.createElement(
      'p',
      {
        key: 'api_key_label',
        style: {marginBottom: '8px'},
      },
      'OpenWeatherMap API key:'
    );
    
    var api_key_input = React.createElement(
      antd.Input,
      {
        key: 'api_key_input',
        onChange: this.onApiKeyChange.bind(this),
        defaultValue: this.props.initialApiKey,
        placeholder: 'API key',
      },
    );
  
    return React.createElement(
      antd.Modal,
      {
        title: 'Settings',
        visible: this.props.visible,
        onOk: this.onClose.bind(this, true),
        onCancel: this.onClose.bind(this, false)
      },
      [theme_div, api_key_label, api_key_input]
    );
  }

}


module.exports = Settings




