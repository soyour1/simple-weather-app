
const React = require('react')
const antd = require('antd')

const ForecastItem = require('./forecast_item.js');

const {getCityDisplayName, getCityLatLonAsString} = require('../city_utils.js')

class CityDisplay extends React.Component {
  
  constructor(props) {
    super(props);
  }
  
  render() {
    var title = '';
    var extra = '';
    var children = [];
    
    if(! this.props.city) {
      title = 'No city selected'; 
    } 
    else {
      title = getCityDisplayName(this.props.city);
      extra = getCityLatLonAsString(this.props.city);
      children = [];
    }
    
    if(this.props.weather) {
      children.push(React.createElement(
        ForecastItem, 
        {
          key: this.props.weather.timestamp,
          showDate: false,
          showHour: false,
          timestamp: this.props.weather.timestamp, 
          weather: this.props.weather.weather,
          temp: this.props.weather.temp,
          pressure: this.props.weather.pressure,
          wind: this.props.weather.wind,
        }, 
        null
      ));
    }
    
    return React.createElement(
      antd.Card,
      {
        title: title,
        extra: extra,
        style: {
          width: '100%',
        },
      },
      children
    );
  }

}


module.exports = CityDisplay;




