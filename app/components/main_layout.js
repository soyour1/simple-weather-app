
const React = require('react')
const antd = require('antd')
const {SettingFilled} = require('@ant-design/icons')

const api = require('../api_requests.js')
const UserPreferences = require('../user_preferences.js')
const css = require('../css.js')

const CitySearchBar = require('./city_search_bar.js')
const CityDisplay = require('./city_display.js')
const ForecastList = require('./forecast_list.js')
const Settings = require('./settings.js')

class MainLayout extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      currentWeather: undefined,
      forecast: [],
      settingsOpen: false,
    };
    
    this.prefs = new UserPreferences();
    
    this.updateCity(this.prefs.getCity());
  }

  // Callback for city selection
  onCitySelect(city) {
    this.prefs.setCity(city);
  
    this.updateCity(city);    
  }
  
  updateCity(city) {
    if(city != undefined) {
      api.dailyForecastRequest(
        city, this.prefs.getApiKey(), this.updateWeatherMapData.bind(this)); 
    } 
  }
  
  // On data received
  updateWeatherMapData(data) {
    if(data) {
      this.setState({
        currentWeather: data.current,
        forecast: data.forecast
      });
    } else {
      this.resetWeatherMapData();
    }
  }
  
  resetWeatherMapData() {
    this.setState({
      currentWeather: undefined,
      forecast: [],
    });
  }
  
  openSettings() {
    this.setState({settingsOpen: true});
  }
  
  onSettingsClosed(apiKey) {
    this.prefs.setApiKey(apiKey);
    
    this.updateCity(this.prefs.getCity());
    
    this.setState({settingsOpen: false}); 
  }

  render() {  
    var city_bar = React.createElement(
      CitySearchBar, 
      {
        key: 'city_bar', 
        onSelect: this.onCitySelect.bind(this),
      }, 
      null
    );
    
    var city_bar_div = React.createElement(
      'div',
      {
        key: 'city_bar_div',
        style: {
          width: 'calc(100% - 40px)', 
          display: 'inline-block',
          marginRight: '8px',
        },
      },
      city_bar
    );
    
    var settings_icon = React.createElement(
      SettingFilled,
      {
        key: 'settings_icon',
        style: {
          fontSize: '24px',
          padding: '4px',
          display: 'inline-block',
        },
        onClick: this.openSettings.bind(this)
      },
      null
    );
    
    var bar_hbox = React.createElement(
      'div', 
      {
        key: 'bar_hbox',
        style: {
          width: '100%', 
          padding: '0px', 
          verticalAlign: 'bottom',
          display: 'flex',
        }
      },
      [city_bar_div, settings_icon]
    );
    
    var city_display = React.createElement(
      CityDisplay,
      {
        key: 'city',
        weather: this.state.currentWeather,
        city: this.prefs.getCity(),
      },
      null
    )
    
    var forecast_list = React.createElement(
      ForecastList,
      {
        key: 'list',
        daysList: this.state.forecast,
      },
      null
    );
    
    var vbox = React.createElement(
      antd.Space, 
      {
        key: 'vbox',
        direction: 'vertical', 
        size: 'large', 
        style: {
          width: '650px',
          textAlign: 'left', 
          padding: '24px',
          margin: '0px auto',
        }
      },
      [bar_hbox, city_display, forecast_list]
    );
    
    var body = React.createElement(
      'div', 
      { 
        key: 'body',
        style: {
          width: '100%', 
          textAlign: 'center',
        }
      },
      [vbox]
    );
    
    var settings = React.createElement(
      Settings,
      {
        key: 'settings',
        visible: this.state.settingsOpen,
        onClose: this.onSettingsClosed.bind(this),
        initialApiKey: this.prefs.getApiKey(),
      },
      null
    )
    
    return [body, settings];
  }

}


module.exports = MainLayout;



