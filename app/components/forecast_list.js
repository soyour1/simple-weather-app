
const React = require('react')
const antd = require('antd')

const ForecastItem = require('./forecast_item.js')

class ForecastList extends React.Component {

  constructor(props) {
    super(props);
  }
  
  createHeaderForecastItem(day) {
    return React.createElement(
      ForecastItem, 
      {
        showDate: true,
        timestamp: day.timestamp, 
        timezone_offset: day.timezone_offset,
        weather: day.weather,
        temp: day.temp,
        pressure: day.pressure,
        wind: day.wind,
      }, 
      null
    );
  }
  
  createHourlyForecastItem(hourly) {
    return React.createElement(
      ForecastItem, 
      {
        showHour: true,
        timestamp: hourly.timestamp, 
        timezone_offset: hourly.timezone_offset,
        weather: hourly.weather,
        temp: hourly.temp,
        pressure: hourly.pressure,
        wind: hourly.wind,
        key: hourly.timestamp,
      }, 
      null
    );
  }
  
  createListItem(day) {
    var hourlies = day.hours.map(function(hours) {
      return this.createHourlyForecastItem(hours);
    }.bind(this));
    
    if(hourlies.length == 0) {
      hourlies = 'No hourly forecast for this day.';
    }
  
    return React.createElement(
      antd.Collapse.Panel, 
      {
        header: this.createHeaderForecastItem(day),
        key: day.dt,
      }, 
      hourlies
    );
  }

  render() {
    
    var items = this.props.daysList.map(function(elem) {
        return this.createListItem(elem);
      }.bind(this));
  
    return React.createElement(
      antd.Collapse,
      {
        ghost: true,
        bordered: true,
      },
      items
    );
  }
  
}

module.exports = ForecastList;



