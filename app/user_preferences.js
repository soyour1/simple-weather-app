
const electron = require('electron')
const fs = require('fs')

class UserPreferences {

  constructor() {    
    this.city = undefined;
    this.apiKey = undefined; 
    
    this.load();
  }
  
  getCity() {
    return this.city;
  }
  
  getApiKey() {
    return this.apiKey;
  }
  
  getFilePath() {
    var path = electron.ipcRenderer.sendSync(
      'synchronous-message', 'userDataPath');
    return path + '/userprefs.json';
  }
  
  load() {
    try {
      var filecontent = fs.readFileSync(this.getFilePath());
      var json = JSON.parse(filecontent);
      
      this.city = json.city;
      this.apiKey = json.api_key;
    } catch(err) {
      console.log('No prefs file');
    }
  }
  
  setCity(city) {
    this.city = city;
    
    this.save();
  }
  
  setApiKey(key) {
    this.apiKey = key;
    
    this.save();
  }
  
  save() {
    var data = {
      city: this.city,
      api_key: this.apiKey,
    };
    
    var str = JSON.stringify(data);
    
    fs.writeFile(this.getFilePath(), str, function(err) {
      console.error(err);
    });
  }

}


module.exports = UserPreferences;




