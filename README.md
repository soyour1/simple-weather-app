# Simple Weather App

A weather application built with [Electron](https://www.electronjs.org/) that
display data provided by [OpenWeatherMap](https://openweathermap.org/). 
The user is required to use his own API key.

This application is provided as a example of use of frameworks, libraries and 
weather API. In particular, this app is _not_ meant nor ready to be used as 
everyday weather app.


## License

The source code is licensed under the [MIT license](https://mit-license.org/).


## Installation

The application does not provide any package installation. 

To run it from the sources, make sure to have [Node.js](https://nodejs.org/) 
and [npm](https://www.npmjs.com/) installed, then run from the repository main 
directory:
```sh
$ npm install
$ npm start
```

