const electron = require('electron')

function createWindow() {
  const win = new electron.BrowserWindow({
    width: 700,
    height: 900,
    webPreferences: {
      nodeIntegration: true
    }
  })
  
  win.loadFile('index.html')
}

// handle events from render process 
electron.ipcMain.on('synchronous-message', function(event, arg) {
  if(arg === 'userDataPath') {
    event.returnValue = electron.app.getPath('userData');
  }
});

electron.app.whenReady().then(createWindow)



